package com.pattern.creational.abstractfactory;

public abstract class AbstractFactory {
    abstract Shape getShape(String shapeType) ;
}
